# **Lumkani Technical Question 1**

Please assume that "today" is 15th January 2019.

Please make sure to ask questions if anything is not clear. 

Solutions can be submitted as as working code or pseudo code - in any suitable format - please email all solutions to paul@lumkani.com 

Please, do not fork this repo. 